/*
    DOM Manipulation
        - manipulate and add interactive events to an html doc using JS.
        
        * If cascading style sheet has a concept of box model, Javascript has its own concept called "document object model".
        
*/

// alert("hello");
// Webpage considered document

// querySelector() this is a method/function that can be use to select a specific element from our document.
console.log(document.querySelector("#txt-first-name"));

// Document refers to the whole page.
console.log(document);

/*
Alternative, that we can user aside from the querySelector inn retrieving elements
    document.getElementById("txt-first-name");
    document.getElementByClassName();
    document.getElementByTagName();
*/

const txtFirstName = document.querySelector("#txt-first-name");
const spanFullName = document.querySelector("#span-full-name");
console.log(txtFirstName);
console.log(spanFullName);

/*
    Event
        click, hover, keypress and many other events

    Event Listeners
        Allows us to let our users interact with our page. Each click or hover is an
        event which can  trigger a function/task

    Syntax:
        selectedElement.addEventListener('event', function);
*/

// keyup event- key release on the keyboard
txtFirstName.addEventListener('keyup', (event) => {

    // innerHTML - is a property of an element which consider all the children of the selected element as a string.
    // malalagyan ng text si spanFullName na galing kay txtFirstName.value
    spanFullName.innerHTML = txtFirstName.value
});

txtFirstName.addEventListener('keyup', (event) => {

    console.log(event);
    // console.log(event.target);
    // console.log(event.target.value)
});

/*
    .value of the input text field
*/

const labelFirstName = document.querySelector("#label-first-name");

// click event - pointer
labelFirstName.addEventListener('click', (e) => {
    console.log(e);
    alert("you click first name label")
})